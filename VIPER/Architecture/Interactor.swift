//
//  Interactor.swift
//  VIPER
//
//  Created by Arpit Dixit on 25/01/22.
//

import Foundation

// Object
// protocol
// ref to presenter

protocol AnyInterator {
    var presenter: AnyPresenter? { get set }
    func getUsers()
}

class UserInterator: AnyInterator {
    var presenter: AnyPresenter?
    
    func getUsers() {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/users") else { return }
        URLSession.shared.dataTask(with: url) { [weak self] data, response, error in
            guard let data = data, error == nil else {
                self?.presenter?.interatorDidFetchUsers(with: .failure(FetchError.failed))
                return
            }
            do {
                let entities = try JSONDecoder().decode([User].self, from: data)
                self?.presenter?.interatorDidFetchUsers(with: .success(entities))
            } catch {
                self?.presenter?.interatorDidFetchUsers(with: .failure(error))
            }
        }.resume()
    }
}
