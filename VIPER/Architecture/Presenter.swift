//
//  Presenter.swift
//  VIPER
//
//  Created by Arpit Dixit on 25/01/22.
//

import Foundation

// Object
// protocol
// ref to interator, router, view

enum FetchError: Error {
    case failed
}

protocol AnyPresenter {
    var router: AnyRouter? { get set }
    var interator: AnyInterator? { get set }
    var view: AnyView? { get set }
    
    func interatorDidFetchUsers(with result: Result<[User], Error>)
}

class UserPresenter: AnyPresenter {
    
    var router: AnyRouter?
    
    var interator: AnyInterator? {
        didSet {
            interator?.getUsers()
        }
    }
    
    var view: AnyView?
    
    func interatorDidFetchUsers(with result: Result<[User], Error>) {
        switch result {
        case .success(let users):
            view?.update(with: users)
        case .failure:
            view?.update(with: "Something went wrong")
        }
    }
}

