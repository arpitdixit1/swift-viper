//
//  Router.swift
//  VIPER
//
//  Created by Arpit Dixit on 25/01/22.
//

import Foundation
import UIKit

// object
// Entry point

typealias EntryPoint = AnyView & UIViewController
protocol AnyRouter {
    var entry: EntryPoint? { get }
    static func start() -> AnyRouter
}

class UserRouter: AnyRouter {
    var entry: EntryPoint?
    
    static func start() -> AnyRouter {
        let router = UserRouter()
        //Assign VIP
        var view: AnyView = UserViewController()
        var interator: AnyInterator = UserInterator()
        var presenter: AnyPresenter = UserPresenter()
        
        view.presenter = presenter
        interator.presenter = presenter
        presenter.router = router
        presenter.view = view
        presenter.interator = interator
        
        router.entry = view as? EntryPoint
        
        return router
    }
}
